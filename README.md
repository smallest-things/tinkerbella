# TinkerBella

## Usage

### Publish

```bash
MQTT_URL="mqtt://mqtt_broker:mqtt_port" ./tk-macos pub "topic" "payload"
```

### Subscribe

```bash
MQTT_URL="mqtt://mqtt_broker:mqtt_port" ./tk-macos sub "topic" 
```

## Building

### Prerequisites
> https://github.com/vercel/pkg
```bash
npm install -g pkg
```

### Build

> it will build for node12-linux-x64, node12-macos-x64, node12-win-x64
```bash
pkg tk.js
```

> TODO: build for RPI 🚧

