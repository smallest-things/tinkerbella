const mqtt = require('mqtt')
const Maybe = require('monet').Maybe

let config = {}

/*
Publish:
MQTT_URL="mqtt://mqttsandbox.home.smart:1883" node tk.js pub house "hey 😃"
MQTT_URL="mqtt://mqttsandbox.home.smart:1883" ./tk-macos pub house "👋 hello world 🌍"

Subscribe:
MQTT_URL="mqtt://mqttsandbox.home.smart:1883" ./tk-macos sub house
*/

// MQTT broker url
Maybe.fromUndefined(process.env.MQTT_URL).cata(
  _ => {
    console.error("😡 you must set MQTT_URL", )
    process.exit()
  },
  mqtt_url => {
    config.mqttUrl = mqtt_url
  }
)

// arguments
let arguments = process.argv.slice(2)

Maybe.fromNull(arguments).cata(
  _ => {
    console.error("😡 you must provide topic and payload", )
    process.exit()
  },
  arguments => {

    Maybe.fromNull(arguments[0]).cata(
      _ => {
        console.error("😡 you must provide an action: pub or sub", )
        process.exit()
      },
      action => {
        if(action=="pub" || action=="sub") {
          config.action = action
        } else {
          console.error("😡 action equals pub or sub", )
          process.exit()
        }
      }
    )

    Maybe.fromNull(arguments[1]).cata(
      _ => {
        console.error("😡 you must provide a topic", )
        process.exit()
      },
      topic => {
        config.topic = topic
      }
    )

    if(config.action=="pub") {
      Maybe.fromNull(arguments[2]).cata(
        _ => {
          console.error("😡 you must provide payload", )
          process.exit()
        },
        payload => {
          config.payload = payload
        }
      )
    }
  }
)
// TODO: handle connection error
const client  = mqtt.connect(config.mqttUrl)

if(config.action=="pub") {
  client.on('connect', _ => {
    client.publish(config.topic, config.payload)
    client.end()
  })
} else {
  // action=="sub"
  client.on('connect', _ => {
    client.subscribe(config.topic, (error) => {
      console.log(error ? `😡 ${error.message}` : `😃 subscribed to ${config.topic} topic`)
    })
  })

  client.on('message',  (topic, message) => {
    console.log(message.toString())
  })
}




